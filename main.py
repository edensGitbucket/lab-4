from sys import argv
import os
from cross_validation import CrossValidation
from knn import KNN
from metrics import accuracy_score
from normalization import *


def load_data():
    """
    Loads data from path in first argument
    :return: returns data as list of Point
    """
    if len(argv) < 2:
        print('Not enough arguments provided. Please provide the path to the input file')
        exit(1)
    input_path = argv[1]

    if not os.path.exists(input_path):
        print('Input file does not exist')
        exit(1)

    points = []
    with open(input_path, 'r') as f:
        for index, row in enumerate(f.readlines()):
            row = row.strip()
            values = row.split(',')
            points.append(Point(str(index), values[:-1], values[-1]))
    return points


def run_knn(points):
    m = KNN(5)
    m.train(points)
    print(f'predicted class: {m.predict(points[0])}')
    print(f'true class: {points[0].label}')
    cv = CrossValidation()
    cv.run_cv(points, 10, m, accuracy_score)


def question1(points):
    m = KNN(1)
    m.train(points)
    predicted = m.predict(points)
    real = [x.label for x in points]
    accuracy = accuracy_score(real, predicted)
    print("accuracy_score is " + str(accuracy))


def question2(points, print_models_accuracies=False):
    max_accuracy = 0
    best_k = 1
    for k in range(1, 31):
        m = KNN(k)
        score = 0
        for index_p in range(len(points)):
            real = [points[index_p]]
            temp = points[:index_p] + points[index_p + 1:]
            m.train(temp)
            predicted = m.predict(real)
            score += accuracy_score(real[0].label, predicted)
        accuracy = score / len(points)
        if accuracy > max_accuracy:
            max_accuracy = accuracy
            best_k = k
        if print_models_accuracies:
            print("{}NN has accuracy of {}".format(k, accuracy))
    if print_models_accuracies:
        print("the best performing classifier is {}NN".format(best_k))
    return best_k


def question3(points):
    print("Question 3:")
    folds = [2, 10, 20]
    k = question2(points)
    print("K=" + str(k))
    m = KNN(k)
    m.train(points)
    cv = CrossValidation()
    for fold in folds:
        print("{}-fold-cross-validation:".format(fold))
        cv.run_cv(points, fold, m, accuracy_score, print_final_score=False, print_fold_score=True)


def question4(points):
    print("Question 4:")
    ks = [5, 7]
    dummy_norm = DummyNormalizer()
    sum_norm = SumNormalizer()
    min_max_norm = MinMaxNormalizer()
    z_norm = ZNormalizer()
    normalizers = [dummy_norm, sum_norm, min_max_norm, z_norm]
    for k in ks:
        print("K="+str(k))
        for norm in normalizers:
            norm.fit(points)
            normalized_points = norm.transform(points)
            m = KNN(k, norm)
            m.train(normalized_points)
            cv = CrossValidation()
            score = cv.run_cv(normalized_points, 2, m, accuracy_score, print_final_score=False, print_fold_score=True)
            print("Accuracy of {} is {}".format(norm.__class__.__name__, score))
            print()


if __name__ == '__main__':
    loaded_points = load_data()
    # run_knn(loaded_points)
    # question1(loaded_points)
    # question2(loaded_points, print_models_accuracies=True)
    question3(loaded_points)
    question4(loaded_points)
